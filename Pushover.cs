﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace AMWD.Net.Push
{
	/// <summary>
	/// Implements the API reference of pushover.net
	/// </summary>
	public class Pushover : IDisposable
	{
		#region Fields

		private static readonly string BaseUrl = "https://api.pushover.net";
		private static readonly string Version = "1";

		private HttpClient client;
		private Dictionary<string, string> sounds = new Dictionary<string, string>();

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Pushover"/> class.
		/// </summary>
		public Pushover()
		{
			client = new HttpClient
			{
				BaseAddress = new Uri(BaseUrl)
			};
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Pushover"/> class.
		/// </summary>
		/// <param name="appToken">The application token.</param>
		/// <param name="userKey">The user key.</param>
		public Pushover(string appToken, string userKey)
			: this()
		{
			AppToken = appToken;
			UserKey = userKey;
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// Gets or sets the application token (Required).
		/// </summary>
		public string AppToken { get; set; }

		/// <summary>
		/// Gets or sets the user key (Required).
		/// </summary>
		public string UserKey { get; set; }

		/// <summary>
		/// Gets or sets the title of the message (max. 250 chars).
		/// </summary>
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets a value that indicates whether the message is HTML encoded.
		/// </summary>
		public bool IsHtmlMessage { get; set; }

		/// <summary>
		/// Gets or sets the message (max. 1024 chars).
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets the retry interval for emergency messages (min. 30 secs).
		/// </summary>
		public TimeSpan RetryInterval { get; set; } = TimeSpan.FromMinutes(1);

		/// <summary>
		/// Gets or sets the time unil the emergency message expires (max. 3h).
		/// </summary>
		public TimeSpan ExpireAfter { get; set; } = TimeSpan.FromMinutes(30);

		/// <summary>
		/// Gets or sets the callback url.
		/// </summary>
		/// <remarks>
		/// The callback url is triggered by the pushover server if an emergency message is acknoledged.
		/// The url has to be available trough the internet.
		/// </remarks>
		public Uri CallbackUrl { get; set; }

		/// <summary>
		/// Gets a list of device names to send the message to.
		/// </summary>
		/// <remarks>
		/// It is possible to define the device to which the message should be sent.
		/// If the list is empty, all devices are addressed.
		/// </remarks>
		public IEnumerable<string> DeviceIds { get; set; }

		/// <summary>
		/// Gets or sets the timestamp of the message (Default: set at <see cref="Send"/> method).
		/// </summary>
		public DateTime Timestamp { get; set; } = DateTime.UtcNow;

		/// <summary>
		/// Gets or sets the priority of the message (Default: <see cref="Priorities.Normal"/>).
		/// </summary>
		public Priorities Priority { get; set; } = Priorities.Normal;

		/// <summary>
		/// Gets or sets a url sent along with the message (max. 512 chars).
		/// </summary>
		public string Url { get; set; }

		/// <summary>
		/// Gets or sets the title of the url sent along with the message (max. 100 chars).
		/// </summary>
		public string UrlTitle { get; set; }

		/// <summary>
		/// Gets or sets the sound to notify the user on message arrival.
		/// </summary>
		public string Sound { get; set; }

		/// <summary>
		/// Gets or sets an image.
		/// </summary>
		public PoImage Image { get; set; }

		#region readonly

		/// <summary>
		/// Gets a list of all available sounds.
		/// </summary>
		public Dictionary<string, string> AvailableSounds
		{
			get
			{
				if (!sounds.Any())
				{
					sounds = GetSounds().Result;
				}
				return sounds;
			}
		}

		/// <summary>
		/// Gets the id of the last request sent to the API.
		/// </summary>
		public string LastRequestId { get; private set; }

		/// <summary>
		/// Gets the limit of messages for the app (identified by the AppToken).
		/// </summary>
		public int? MessageLimit { get; private set; }

		/// <summary>
		/// Gets the remaining messages for the app.
		/// </summary>
		public int? RemainingMessages { get; private set; }

		/// <summary>
		/// Gets the time when the remaining messages are reset.
		/// </summary>
		public DateTime? ResetAt { get; private set; }

		#endregion readonly

		#endregion Properties

		#region Public methods

		/// <summary>
		/// Validates the user's key along with the application's token.
		/// </summary>
		/// <returns>true if the key is valid, otherwise false.</returns>
		public async Task<bool> ValidateKey(CancellationToken cancellationToken = default(CancellationToken))
		{
			if (isDisposed)
			{
				throw new ObjectDisposedException(GetType().FullName);
			}
			if (string.IsNullOrWhiteSpace(AppToken))
			{
				throw new ArgumentNullException(nameof(AppToken));
			}
			if (string.IsNullOrWhiteSpace(UserKey))
			{
				throw new ArgumentNullException(nameof(UserKey));
			}

			var param = new Dictionary<string, string>
			{
				{ "token", AppToken },
				{ "user", UserKey }
			};

			var response = await Request(RequestMethods.Post, "users/validate.json", param, cancellationToken);

			LastRequestId = response.RequestId;
			MessageLimit = response.Limit.MessageLimit;
			RemainingMessages = response.Limit.RemainingMessages;
			ResetAt = response.Limit.ResetAt;

			return response.IsSuccess;
		}

		/// <summary>
		/// Sends the message.
		/// </summary>
		/// <returns>The response parameters.</returns>
		public async Task<Response> Send(CancellationToken cancellationToken = default(CancellationToken))
		{
			if (isDisposed)
			{
				throw new ObjectDisposedException(GetType().FullName);
			}
			if (string.IsNullOrWhiteSpace(AppToken))
			{
				throw new ArgumentNullException(nameof(AppToken));
			}
			if (string.IsNullOrWhiteSpace(UserKey))
			{
				throw new ArgumentNullException(nameof(UserKey));
			}
			if (string.IsNullOrWhiteSpace(Message))
			{
				throw new ArgumentNullException(nameof(Message));
			}

			if (Timestamp == DateTime.MinValue)
			{
				Timestamp = DateTime.UtcNow;
			}

			var param = new Dictionary<string, string>
			{
				{ "token", AppToken },
				{ "user", UserKey },
				{ "message", Message },
				{ "html", IsHtmlMessage ? "1" : "0" },
				{ "title", Title },
				{ "timestamp", ToUnixTimestamp(Timestamp).ToString() },
				{ "callback", CallbackUrl?.ToString() ?? "" },
				{ "priority", ((int)Priority).ToString() },
			};

			if (Priority == Priorities.Emergency)
			{
				param.Add("expire", ExpireAfter.TotalSeconds.ToString());
				param.Add("retry", RetryInterval.TotalSeconds.ToString());
			}
			if (DeviceIds?.Any() == true)
			{
				param.Add("device", string.Join(",", DeviceIds));
			}
			if (!string.IsNullOrWhiteSpace(Sound))
			{
				param.Add("sound", Sound);
			}
			if (!string.IsNullOrWhiteSpace(Url))
			{
				param.Add("url", Url);
				param.Add("url_title", UrlTitle);
			}

			var response = await Request(RequestMethods.Post, "messages.json", param, cancellationToken);

			LastRequestId = response.RequestId;
			MessageLimit = response.Limit.MessageLimit;
			RemainingMessages = response.Limit.RemainingMessages;
			ResetAt = response.Limit.ResetAt;

			return response;
		}

		/// <summary>
		/// Retrieves the limits for the application.
		/// </summary>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public async Task<Limits> GetLimits(CancellationToken cancellationToken = default(CancellationToken))
		{
			if (isDisposed)
			{
				throw new ObjectDisposedException(GetType().FullName);
			}
			if (string.IsNullOrWhiteSpace(AppToken))
			{
				throw new ArgumentNullException(nameof(AppToken));
			}

			var param = new Dictionary<string, string>
			{
				{ "token", AppToken }
			};

			var response = await Request(RequestMethods.Get, "limits.json", param, cancellationToken);

			LastRequestId = response.RequestId;
			MessageLimit = response.Limit.MessageLimit;
			RemainingMessages = response.Limit.RemainingMessages;
			ResetAt = response.Limit.ResetAt;

			return response.Limit;
		}

		#endregion Public methods

		#region Private methods

		private async Task<Dictionary<string, string>> GetSounds()
		{
			if (isDisposed)
			{
				throw new ObjectDisposedException(GetType().FullName);
			}
			if (string.IsNullOrWhiteSpace(AppToken))
			{
				throw new ArgumentNullException(nameof(AppToken));
			}

			var param = new Dictionary<string, string>
			{
				{ "token", AppToken }
			};

			var response = await Request(RequestMethods.Get, "sounds.json", param);

			LastRequestId = response.RequestId;
			MessageLimit = response.Limit.MessageLimit;
			RemainingMessages = response.Limit.RemainingMessages;
			ResetAt = response.Limit.ResetAt;

			var dict = new Dictionary<string, string>();
			if (response.IsSuccess)
			{
				foreach (JProperty sound in response.Raw["sounds"])
				{
					dict.Add(sound.Name, sound.Value.ToString());
				}
			}

			return dict;
		}

		private async Task<Response> Request(RequestMethods method, string path, Dictionary<string, string> param = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (string.IsNullOrWhiteSpace(path))
			{
				throw new ArgumentNullException(nameof(path));
			}

			HttpResponseMessage response = null;
			switch (method)
			{
				case RequestMethods.Get:
					var queryString = "";
					if (param?.Any() == true)
					{
						queryString = "?" + string.Join("&", param.Select(kvp => kvp.Key + "=" + kvp.Value));
					}
					response = await client.GetAsync($"/{Version}/{path.Trim('/')}{queryString}", cancellationToken);
					break;
				case RequestMethods.Post:
					if (param == null)
					{
						param = new Dictionary<string, string>();
					}
					using (var form = new MultipartFormDataContent($"----{DateTime.UtcNow.Ticks}"))
					{
						foreach (var kvp in param)
						{
							if (kvp.Value != null)
							{
								form.Add(new StringContent(kvp.Value), kvp.Key);
							}
						}
						if (Image?.Bytes?.Length > 0)
						{
							if (string.IsNullOrWhiteSpace(Image.Type))
							{
								throw new ArgumentException("Image type not set.");
							}

							var bac = new ByteArrayContent(Image.Bytes);
							bac.Headers.ContentType = new MediaTypeHeaderValue(Image.Type);
							bac.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
							form.Add(bac, "attachment");
						}
						response = await client.PostAsync($"/{Version}/{path.Trim('/')}", form, cancellationToken);
					}
					break;
				default:
					throw new NotSupportedException($"Request method '{method}' not known");
			}

			var json = await response.Content.ReadAsStringAsync();
			var result = new Response(json);
			result.ParseHeaders(response.Headers);
			return result;
		}

		private static int ToUnixTimestamp(DateTime datetime)
		{
			return (int)datetime.ToUniversalTime()
				.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc))
				.TotalSeconds;
		}

		private static DateTime FromUnixTimestamp(int unix)
		{
			return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)
				.AddSeconds(unix);
		}

		#endregion Private methods

		#region IDisposable implementation

		private bool isDisposed;

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
		}

		private void Dispose(bool disposing)
		{
			if (isDisposed)
			{
				return;
			}

			isDisposed = true;
			client?.Dispose();
			client = null;
		}

		#endregion IDisposable implementation

		#region Enums

		/// <summary>
		/// Represents the possible priorities for an pushover message.
		/// </summary>
		public enum Priorities
		{
			/// <summary>
			/// Normal priority.
			/// </summary>
			Normal = 0,
			/// <summary>
			/// Lower priority.
			/// </summary>
			Lower = -1,
			/// <summary>
			/// Lowest priority.
			/// </summary>
			Lowest = -2,
			/// <summary>
			/// Higher priority.
			/// </summary>
			Higher = 1,
			/// <summary>
			/// Highest priority. Message has to be acknowledged.
			/// </summary>
			Emergency = 2
		}

		private enum RequestMethods
		{
			Get,
			Post
		}

		#endregion Enums

		#region Classes

		/// <summary>
		/// Contains the response content.
		/// </summary>
		public class Response
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="Pushover.Response"/> class.
			/// </summary>
			public Response()
			{ }

			/// <summary>
			/// Initializes a new instance of the <see cref="Pushover.Response"/> class.
			/// </summary>
			/// <param name="json">JSON serialized data.</param>
			public Response(string json)
				: this(JObject.Parse(json))
			{ }

			/// <summary>
			/// Initializes a new instance of the <see cref="Pushover.Response"/> class.
			/// </summary>
			/// <param name="jObject">Data of the response.</param>
			public Response(JObject jObject)
			{
				Raw = jObject;

				Status = jObject["status"].Value<int>();
				RequestId = jObject["request"].Value<string>();

				if (jObject.ContainsKey("receipt"))
				{
					Receipt = jObject["receipt"].Value<string>();
				}
				if (jObject.ContainsKey("errors"))
				{
					foreach (var error in jObject["errors"] as JArray)
					{
						Errors.Add(error.Value<string>());
					}
				}

				if (jObject.ContainsKey("limit"))
				{
					Limit = new Limits
					{
						MessageLimit = jObject["limit"].Value<int>(),
						RemainingMessages = jObject["remaining"].Value<int>(),
						ResetAt = FromUnixTimestamp(jObject["reset"].Value<int>())
					};
				}

				if (jObject.ContainsKey("app_limits"))
				{
					Limit = jObject["app_limits"].Value<Limits>();
				}
			}

			/// <summary>
			/// Gets or sets the status.
			/// </summary>
			[JsonProperty("status")]
			public int Status { get; set; }

			/// <summary>
			/// Gets a value indicating whether the request was successful.
			/// </summary>
			[JsonIgnore]
			public bool IsSuccess => Status == 1;

			/// <summary>
			/// Gets or sets the request id.
			/// </summary>
			[JsonProperty("request")]
			public string RequestId { get; set; }

			/// <summary>
			/// Gets the list of errors (if IsSuccess == false)
			/// </summary>
			[JsonProperty("errors")]
			public List<string> Errors { get; } = new List<string>();

			/// <summary>
			/// Gets or sets the receipt url if <see cref="Pushover.Priority"/> was <see cref="Pushover.Priorities.Emergency"/>.
			/// </summary>
			[JsonProperty("receipt")]
			public string Receipt { get; set; }

			/// <summary>
			/// Gets or sets the application limits (if availble).
			/// </summary>
			[JsonProperty("app_limits")]
			public Limits Limit { get; set; }

			/// <summary>
			/// Gets the raw JSON object.
			/// </summary>
			public JToken Raw { get; private set; }

			/// <summary>
			/// Serializes this instance to JSON.
			/// </summary>
			/// <returns></returns>
			public string ToJson()
			{
				return JsonConvert.SerializeObject(this);
			}

			/// <summary>
			/// Parses the headers for limits.
			/// </summary>
			/// <param name="headers">The headers of the response.</param>
			public void ParseHeaders(HttpResponseHeaders headers)
			{
				if (headers.TryGetValues("X-Request-Id", out IEnumerable<string> outRequest))
				{
					RequestId = outRequest.First();
				}

				if (headers.TryGetValues("X-Limit-App-Limit", out IEnumerable<string> outLimit))
				{
					if (Limit == null)
					{
						Limit = new Limits();
					}

					Limit.MessageLimit = int.Parse(outLimit.First());
				}

				if (headers.TryGetValues("X-Limit-App-Remaining", out IEnumerable<string> outRemaining))
				{
					if (Limit == null)
					{
						Limit = new Limits();
					}

					Limit.RemainingMessages = int.Parse(outRemaining.First());
				}

				if (headers.TryGetValues("X-Limit-App-Reset", out IEnumerable<string> outReset))
				{
					if (Limit == null)
					{
						Limit = new Limits();
					}

					Limit.ResetAt = FromUnixTimestamp(int.Parse(outReset.First()));
				}
			}
		}

		/// <summary>
		/// Contains the application limits.
		/// </summary>
		public class Limits
		{
			/// <summary>
			/// Gets or sets the limit.
			/// </summary>
			public int MessageLimit { get; set; }

			/// <summary>
			/// Gets or sets the remaining messages.
			/// </summary>
			public int RemainingMessages { get; set; }

			/// <summary>
			/// Gets or sets the time when the remaining messages are reset.
			/// </summary>
			public DateTime ResetAt { get; set; }
		}

		/// <summary>
		/// Contains the data of an image for pushover.
		/// </summary>
		public class PoImage
		{
			/// <summary>
			/// Gets or sets an image content.
			/// </summary>
			public byte[] Bytes { get; set; }

			/// <summary>
			/// Gets or sets the image type, e.g. image/jpeg.
			/// </summary>
			public string Type { get; set; }
		}

		#endregion Classes
	}
}
